package com.example.bunha.mycontact;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class AddContact extends AppCompatActivity {

    EditText etName;
    EditText etNumber1;
    EditText etNumber2;
    Button btnSubmit;
    PersonDbHelper personDbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_addcontact);

        etName = (EditText) findViewById(R.id.etName);
        etNumber1= (EditText) findViewById(R.id.etNumber1);
        etNumber2= (EditText) findViewById(R.id.etNumber2);
        btnSubmit= (Button) findViewById(R.id.btnSave);

        personDbHelper = new PersonDbHelper(this);

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Person p = new Person(etName.getText().toString(), etNumber1.getText().toString(),etNumber2.getText().toString());
                if (personDbHelper.addPerson(p)){
                    Toast.makeText(AddContact.this, "Insert Successfully", Toast.LENGTH_SHORT).show();
                    finish();

                } else{
                    Toast.makeText(AddContact.this, "Failed....!", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
