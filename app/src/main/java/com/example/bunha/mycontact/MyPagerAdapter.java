package com.example.bunha.mycontact;

import android.content.Context;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by bunha on 8/2/2017.
 */

public class MyPagerAdapter extends FragmentPagerAdapter {

    List<Tab> tabList;
    Context context;

    public MyPagerAdapter(FragmentManager fm, Context context) {
        super(fm);
        tabList = new ArrayList<>();
        this.context = context;
    }

    @Override
    public Fragment getItem(int position) {
        return tabList.get(position).getFragment();
    }

    @Override
    public int getCount() {
        return tabList.size();
    }

    public void addTab(Tab tab) {
        tabList.add(tab);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        //return tabList.get(position).getTitle();
        return tabList.get(position).getTitle();
    }


    private View getCustomView(int position) {
        Tab tab = tabList.get(position);
        View v = LayoutInflater.from(context).inflate(R.layout.custom_tab, null);
        TextView tvTitle = (TextView) v.findViewById(R.id.tvTitle);
        tvTitle.setText(tab.getTitle());
        return v;
    }
}
